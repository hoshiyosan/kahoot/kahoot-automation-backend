help:
	@echo toto

init:
	pip install --user virtualenv
	virtualenv -p /usr/bin/python3 .env


install:
	.env/bin/pip install -r requirements.txt

run:
	.env/bin/python run.py