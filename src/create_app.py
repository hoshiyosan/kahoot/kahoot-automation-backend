from flask import Flask
from .addons import api, kahoot
from .resources.kahoot import kahoot_ns

def create_app(app_name):
	app = Flask(app_name)

	api.init_app(app)
	api.add_namespace(kahoot_ns)

	kahoot.init_app(app)

	return app
