import uuid
from random import randint
from .strategy import Strategy

class Player:
	def __init__(self, game_pin, username=None):
		self.id = uuid.uuid4().hex[:10]
		self.game_pin = game_pin
		self.username = uuid.uuid4().hex[:10]
		self.strategy = Strategy.RANDOM

	def json(self):
		return {
			"id": self.id,
			"username": self.username,
			"game_pin": self.game_pin,
		}

	def pick_answer(self):
		if self.strategy == Strategy.RANDOM:
			return randint(0, 3)

	def __str__(self):
		return "(user) %s in session %s" % (self.username, self.game_pin)

