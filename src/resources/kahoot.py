from flask_restplus import Namespace, Resource
from ..addons import kahoot

kahoot_ns = Namespace('kahoot', 'kahoot description', path='/kahoot')

@kahoot_ns.route('/player/<string:player_id>')
class PlayerResource(Resource):
	def get(self, player_id):
		return kahoot.dump_player(player_id)

@kahoot_ns.route('/join/<int:game_pin>')
class KahootResource(Resource):
	def get(self, game_pin):
		player_id = kahoot.add_player(game_pin)
		return {
			"player_id": player_id
		}
