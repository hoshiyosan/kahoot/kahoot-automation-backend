from threading import Thread
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from ..models.player import Player
import time

class Kahoot(Thread):
	def __init__(self, app=None):
		Thread.__init__(self)
		self.players = []
		self.pending_players = []

		if app:
			self.init_app(app)

	def wait(self, *locator):
		return WebDriverWait(self.browser, 10).until(
			EC.presence_of_element_located(locator)
		)

	def init_app(self, app):
		self.app = app
		self.start()

	def dump_player(self, pid):
		matches = list(filter(lambda p: p.id==pid, self.players))
		if len(matches) == 1:
			return matches[0].json()
		else:
			raise ValueError("Error looking for player with pid %s"%pid)

	def add_player(self, game_pin, username=None):
		self.pending_players.append(
			Player(game_pin, username)
		)
		return self.pending_players[-1].id
	

	def open_tab(self, page_url=None):
		old_tabs = set(self.browser.window_handles)
		self.browser.execute_script('window.open()')
		current_tabs = set(self.browser.window_handles)
		tab_id = list(current_tabs - old_tabs)[0]

		if page_url:
			self.browser.switch_to_window(tab_id)
			self.browser.get(page_url)

		return tab_id

	def send_keys(self, locator, text):
		element = self.wait(*locator)
		element.clear()
		element.send_keys(text)
	

	def play(self, player):
		self.browser.switch_to_window(player.tab_id)

		try:
			if self.browser.current_url == 'https://kahoot.it/v2':
				self.send_keys(('id', 'game-input'), player.game_pin)
				self.wait('xpath', '//button[@data-functional-selector="join-game-pin"]').click()

			elif self.browser.current_url == "https://kahoot.it/v2/join":
				self.send_keys(('id', 'nickname'), player.username)
				self.wait('xpath', '//button[@data-functional-selector="join-button-username"]').click()
			
			elif self.browser.current_url == "https://kahoot.it/v2/gameblock":
				answer_index = player.pick_answer()
				answer_xpath = '//button[@data-functional-selector="answer answer-%s"]'%answer_index
				print(answer_xpath)
				self.wait('xpath', answer_xpath).click()
		except Exception as e:
			pass


	def run(self):
		options = Options()
		options.headless = True
		self.browser = webdriver.Firefox(
			executable_path='./assets/geckodriver', 
			log_path='/dev/null', 
			options=options)
		
		print(' * kahoot backend just started')
		while True:
			for i in range(len(self.pending_players)):
				player = self.pending_players.pop(0)
				player.tab_id = self.open_tab('https://kahoot.it/v2')
				self.players.append(player)
			
			for player in self.players:
				self.play(player)

			time.sleep(0.1)
