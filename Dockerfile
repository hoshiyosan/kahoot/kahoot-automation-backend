# This file is a template, and might need editing before it works on your project.
FROM ubuntu
#python:3.6

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends\
    && apt-get install -y firefox\
    && apt-get install -y python3 python3-pip

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip3 install --no-cache-dir -r requirements.txt
COPY . /usr/src/app

# env vars
ENV FLASK_ENV=production

#
RUN python3 -V
RUN firefox -v

# For Django
EXPOSE 8000
#CMD ["gunicorn", "-b", "0.0.0.0:8000", "run:app"]
CMD ["gunicorn", "-b", "0.0.0.0:8000", "run:app"]